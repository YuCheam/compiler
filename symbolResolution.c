# include "symbolTable.h"
# include "lexer.h"
# include "symbolResolution.h"
# include "parser.h"

/* init_unknownId()
 * Parameters: token *t, char *type
 * Return: Pointer to unknown identifier */
unknown_id *init_unknownId(token t, char *type) {
  unknown_id *id = (unknown_id *)malloc(sizeof(unknown_id));

  id->t = t;
  id->type = strdup(type);
  id->s = (sym **) malloc(sizeof(sym *));

  return id;
}

/* Function: addUnknown() -- add unknown id to unknown array
 *                           checks for duplicates
 * Parameters: unknown_id *id, unknown_arr *arr
 * Return: void */
void addUnknown(void *id, unknown_arr *arr, enum unk_case u_case) {
  int total;
  if (u_case == ID) {
    unknown_id *u_id= (unknown_id *) id;
    total = arr->total[ID];
    if (total == 0) {
      arr->unknown[total] = id;
    } else {
      int i;
      
      for(i=0; i<total; i++) {
        unknown_id *a = arr->unknown[i];
        char *name = a->t.lexeme;
        char *type = a->type;
        
        // Check if unknown symbol already is in array,
        // set id to point at symbol
        if (strcmp(name, u_id->t.lexeme) == 0 && strcmp(u_id->type, type) == 0) {
          u_id->s = a->s;
          return;
        }
      }
      
      arr->unknown[total] = u_id;
    }
    
    arr->total[ID] += 1;
  } else if (u_case == TYPE) {
    unk_type *ut_id = (unk_type *) id;
    total = arr->total[TYPE];
    
    arr->unkType[total] = ut_id;
    arr->total[TYPE] += 1;

  } else if (u_case == ARGNUM) {
    unk_nargs *u_id = (unk_nargs *) id;
    total = arr->total[ARGNUM];

    if (total == 0) {
        arr->unkArgs[total] = u_id;
        arr->total[ARGNUM] += 1;
    } else {
        int i;
        for(i=0; i<total; i++) {
            unknown_id *a = arr->unkArgs[i]->id;
            char *name = a->t.lexeme;
            char *type = a->type;

            // Check if item is already in array
            if (strcmp(name, a->t.lexeme) == 0 && strcmp(a->type, type) == 0) {
                if (u_id->args == arr->unkArgs[i]->args) return;
            }
        }
        arr->unkArgs[total] = u_id;
        arr->total[ARGNUM] += 1;
    }
  }
};

/* Function: resolve() -- resolves all unknown symbols and gives errMsg if
 *           there are issues with scoping or has not been previously declared
 * Paraemters: unknown_arr *arr (list of unknown)
 *             symTb *tb (global symbol table)
 * Return: void */
void resolve(unknown_arr *arr, symTb *tb) {
  unknown_id *id;
  int i;
  sym *s;

  for(i=0; i<arr->total[ID]; i++) {
    id = arr->unknown[i];
    sym **ps = id->s;

    if (strcmp(id->type, "id") == 0) {
      s = lookUp(id->t.lexeme, tb);
      if(s == NULL) {
        char *errMsg = "Class not declared previously";
        error(id->t, errMsg);
      } else {
        *ps = s;
      }
    } else {
      sym *class = lookUp(id->type, tb);
      
      if(class == NULL) {
        char *errMsg = "Class not declared previously";
        error(id->t, errMsg);
      } else {
        s = lookUp(id->t.lexeme, class->scope);
        if(s == NULL) {
          char *errMsg = "Field or function of class does not exist";
          error(id->t, errMsg);
        } else {
          *ps = s;
        }
      }
    }
  }

};

/* Function: resolveType() -- Resolve type conflicts
 * Parameters: unk_type* arr[], int total
 * Return: void */
void resolveType(unknown_arr  *arr) {
  int i;
  unk_type *item;
  sym *s;
  int total = arr->total[TYPE];

  for (i=0; i<total; i++) {
    item = arr->unkType[i];
    s =  *item->id->s;

    switch(s->kind) {
      case CONSTRUCTOR:
      case FUNCTION:
      case METHOD:
        if(strcmp(s->retType, item->type) != 0) {
          char *errMsg = "LHS and RHS assignment incorrect";
          sem_error(item->id->t, errMsg);
        }
        break;
      default:
        if (strcmp(s->dtype, item->type) != 0) {
          char *errMsg = "LHS and RHS assignment not the same type";
          sem_error(item->id->t, errMsg);
        }
    }
  }
};

/* Function: resolveArgs()
 * Parameters: unknown_arr *
 * Purpose: Resolve argument numbers */
 void resolveArgs(unknown_arr *arr) {
  int i;
  int total = arr->total[ARGNUM];
  unk_nargs *item;
  sym *s;

  for(i=0; i<total; i++) {
    item = arr->unkArgs[i];
    s = *item->id->s;
    
    if (s->args != item->args) {
      char errMsg[100];
      sprintf(errMsg, "Expected %d args, but got %d args", s->args, item->args);
      sem_error(item->id->t, errMsg);
    }
  }
 }
