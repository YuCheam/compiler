# include "lexer.h"
# include "parser.h"
# include "symbolTable.h"
# include "symbolResolution.h"

token t;
int type_counter = 0;

char *class_declar[] = {"static", "field"};
char *sub_declar[] = {"constructor", "function", "method"};
char *type_array[] = {"int", "char", "boolean", "identifier"};
char *stmt_array[] = {"var", "let", "if", "while", "do", "return"};
char *expr_array[] = {"-", "~", "(", "true", "false", "null", "this"};
char *api_array[] = {"Math", "String", "Array", "Output", "Screen", "Keyboard",
    "Memory", "Sys"};
char *newType[100];

symTb *glbSymTb;
symTb *currSymTb;
unknown_arr *unknown_ids;

/* Function cmp(): looks for word in array
 * Return: 1 if found, 0 if not */
int cmp(char *word, char *array[], int len) {
    int i;
    for (i=0; i<len; i++) {
        if (!strcmp(array[i], word)) return 1;
    }
    return 0;
}

char* type() {
    t = getNextToken();
    if (!strcmp(t.lexeme, "int")) {
        return "int";
    } else if (!strcmp(t.lexeme, "char")) {
        return "char";
    } else if (!strcmp(t.lexeme, "boolean")) {
        return "boolean";
    } else if (!strcmp(t.lexeme, "identifier")) {
        return "identifier";
    } else if (cmp(t.lexeme, newType, type_counter)) { 
        return t.lexeme;
    } else if(cmp(t.lexeme, api_array, 8)) {
        return t.lexeme;
    } else {
        //char *errMsg = "Expected type declaration";
        //error(t, errMsg);
        // Add to unknown arr
        addUnknown(init_unknownId(t, "id"), unknown_ids, ID);
        return t.lexeme;
    }

};

/* Function: operand()
 * return: char *dtype = data type 
 * TODO: Still working on this*/
unk_type *operand() {
    t = getNextToken();
    char *dtype = "";
    char *name;
    unk_type *utype = malloc(sizeof(unk_type));
    unknown_id *u_id = NULL;
    sym *s_id = NULL;
    int num_args = -1;

    if (t.type == number) {
        //Is number
        dtype = "int";
    } else if (t.type == identifier) {
        // Check if it's been declared
        sym *s = lookUp(t.lexeme, currSymTb);
        if (s  == NULL) {
            // Differentiate based upon whether field of class or not
            if(strcmp(peekNextToken().lexeme, ".") == 0) {
                addUnknown(init_unknownId(t, "id"), unknown_ids, ID);
                name = t.lexeme;
            } else {
                char *temp = findClass(currSymTb);
                u_id  = init_unknownId(t, temp);

                addUnknown(u_id, unknown_ids, ID);
                dtype = "unknown";
                utype->id = u_id;
            }
        } else { // Has been previously declared
            if (s->kind <= 7 && s->kind >= 5) {
                name = s->dtype;
                s_id = s;
                num_args = s->args;
            } else if (s->kind <= 3 && s->kind >= 0) {
                name = s->dtype;
            } else {
               name = s->name;
            }
            dtype = name;

            // Check if variable is initialized
            if (s->kind == VAR && s->init == 0) {
                char errMsg[100];
                sprintf(errMsg, "Variable, %s, is uninitialized, cannot use uninitialized var", t.lexeme);
                error(t, errMsg);
            }
        }

        t = peekNextToken();
        if(!strcmp(t.lexeme, ".")) {
            getNextToken();

            t = getNextToken();
            if(t.type == identifier) {
                sym *st = lookUp(name, glbSymTb);
                sym *s = st == NULL ? NULL : lookUp(t.lexeme, st->scope);

                if(s == NULL) {
                    // Add to unknown list
                    u_id = init_unknownId(t, name);
                    addUnknown(u_id, unknown_ids, ID);
                    dtype = "unknown";
                    utype->id = u_id;
                } else {
                    switch (s->kind) {
                        case FUNCTION:
                        case CONSTRUCTOR:
                        case METHOD:
                            dtype = s->retType;
                            num_args = s->args;
                            s_id = s;
                            break;
                        default:
                            dtype = s->dtype;
                    }
                }
            } else {
                char *errMsg = "Expected an identifier";
                error(t, errMsg);
            }
        }

        t = peekNextToken();
        if(!strcmp(t.lexeme, "[") || !strcmp(t.lexeme, "(")) {
            getNextToken();
            if(!strcmp(t.lexeme, "[")) {
                unk_type *type = expression();
                // Check that returned type is int
                if (strcmp(type->type, "int") != 0 && strcmp(type->type, "char") != 0) {
                    char *errMsg = "Array indices need integer values";
                    sem_error(t, errMsg);
                }

                t = getNextToken();
                if(!strcmp(t.lexeme, "]")) {
                    // is symbol ]
                } else {
                    char *errMsg = "Expected ']' symbol";
                    error(t, errMsg);
                }

            } else {
                // Check type of arguments and number of arguments
                void *p;
                if (u_id) {p = u_id; } else {p = s_id;}
                int isKnown = u_id != NULL ? 0 : 1;
                int num_expr = expressionList(p, isKnown);
                if (num_args == -1) {
                    unk_nargs *unk = (unk_nargs *)malloc(sizeof(unk_nargs));
                    unk->id = u_id;
                    unk->args = num_expr;
                    addUnknown(unk, unknown_ids, ARGNUM);
                } else if (num_expr != num_args) {
                    char errMsg[200];
                    sprintf(errMsg, "Expected %d num of arguments but got %d", num_args, num_expr);
                    sem_error(t, errMsg);
                }

                t = getNextToken();
                if(!strcmp(t.lexeme, ")")) {
                    // is symbol )
                } else {
                    char *errMsg = "Expected ')' symbol";
                    error(t, errMsg);
                }
            }
        }

    } else if (!strcmp(t.lexeme, "(")) {
        utype = expression();
        dtype = utype->type;
        t = getNextToken();
        if(!strcmp(t.lexeme, ")")) {
            // is symbol )
        } else {
            char *errMsg = "Expected ')' symbol";
            error(t, errMsg);
        }
    } else if (t.type == stringLiteral) {
        // Is type stringLiteral
    } else if (!strcmp(t.lexeme, "true")) {
        dtype = "boolean";
        // Is true boolean
    } else if (!strcmp(t.lexeme, "false")) {
        dtype = "boolean";
        // Is false boolean
    } else if (!strcmp(t.lexeme, "null")) {
        // is null keyword
        dtype = "null";
    } else if (!strcmp(t.lexeme, "this")) {
        dtype = findClass(currSymTb);
        // is this keyword
    } else {
        char *errMsg = "Expected an operand";
        error(t, errMsg);
    }

    utype->type = strdup(dtype);
    return utype;
}

unk_type *factor() {
    t = peekNextToken();
    unk_type *dtype;

    if (!strcmp(t.lexeme, "-")) {
        getNextToken();
        // Is - symbol
    } else if(!strcmp(t.lexeme, "~")) {
        getNextToken();
        // Is ~ symbol
    } 

    dtype = operand();
    return dtype;
};

unk_type *term(){
    unk_type *dtype = factor();

    t = peekNextToken();
    char *op_arr[] = {"*", "/"};
    while(cmp(t.lexeme, op_arr, 2)) {
        getNextToken();
        factor();
        t = peekNextToken();
    }

    return dtype;
};

unk_type *arithmeticExpression(){
    unk_type *dtype = term();

    t = peekNextToken();
    char *op_arr[] = {"-", "+"};
    while(cmp(t.lexeme, op_arr, 2)) {
        getNextToken();
        term();
        t = peekNextToken();
    }

    return dtype;
};

unk_type *relationalExpression(){
    unk_type *dtype = arithmeticExpression();

    t = peekNextToken();
    char *op_arr[] = {"=", ">", "<"};
    while(cmp(t.lexeme, op_arr, 3)) {
        getNextToken();
        arithmeticExpression();
        t = peekNextToken();
        dtype->type = "boolean";
    }

    return dtype;
};

int expressionList(void *p, int isKnown) {
    int total = 0;
    unk_type *ut_id;

    t = peekNextToken();
    if(!(t.type == number || t.type == identifier || t.type == stringLiteral ||
                cmp(t.lexeme, expr_array, 7))) return total;

    // Check whether expression is the correct type
    ut_id = expression();
    total += 1;

    t = peekNextToken();
    while(!strcmp(t.lexeme, ",")) {
        getNextToken();
        expression();
        total += 1;
        t = peekNextToken();
    }

    return total;
};

void subroutineCall() {
    t = getNextToken();
    char *className;
    int num_args = -1;
    unknown_id *u_id = NULL;
    sym *s_id = NULL;

    if (t.type == identifier) {
        // Check if previously declared
        sym *s = lookUp(t.lexeme, currSymTb);

        if(s == NULL) {
            // Different if method vs function
            if(strcmp(peekNextToken().lexeme, ".") == 0) {
                addUnknown(init_unknownId(t, "id"), unknown_ids, ID);
                className = t.lexeme;

            } else {
                char *temp = findClass(currSymTb);
                u_id = init_unknownId(t, temp);
                addUnknown(u_id, unknown_ids, ID);
            }
        } else {
            className = s->kind == CLASS ? s->name : s->dtype;
            num_args = s->kind == CLASS ? -1 :  s->args;
            s_id = s->kind == CLASS ? NULL : s;
        }
    } else {
        char *errMsg = "Expected an identifier";
        error(t, errMsg);
    }

    t = peekNextToken();
    if (!strcmp(t.lexeme, ".")) {
        getNextToken();
        t = getNextToken();
        if (t.type == identifier) {
            sym *s = lookUp(t.lexeme, findTopScope(currSymTb));
            if(s == NULL) {
                u_id = init_unknownId(t, className);
                addUnknown(u_id, unknown_ids, ID);
            } else {
                num_args = s->args;
                s_id = s;
            }
        } else {
            char *errMsg = "Expected an identifier";
            error(t, errMsg);
        }
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "(")) {
        // is ( symbol
    } else {
        char *errMsg = "Expected '(' symbol";
        error(t, errMsg);
    }

    // Check whether argument numbers match
    int num_expr;
    if (s_id != NULL) {
        num_expr = expressionList(s_id, 1);
    } else {
        num_expr = expressionList(u_id, 0);
    }
    if (num_args == -1) {
        unk_nargs *unk= (unk_nargs *)malloc(sizeof(unk_nargs));
        unk->id = u_id;
        unk->args = num_expr;
        addUnknown(unk, unknown_ids, ARGNUM);
    } else if (num_expr != num_args) {
        char errMsg[200];
        sprintf(errMsg, "Expected %d arguments, but got %d", num_args, num_expr);
        sem_error(t, errMsg);
    }


    t = getNextToken();
    if (!strcmp(t.lexeme, ")")) {
        // is ) symbol
    } else {
        char *errMsg = "Expected ')' symbol";
        error(t, errMsg);
    }

};

/* Function: expression()
 * Return: type of expression */
unk_type *expression(){
    unk_type *dtype = relationalExpression();

    t = peekNextToken();

    char *op_arr[] = {"&", "|"};
    while(cmp(t.lexeme, op_arr, 2)) {
        getNextToken();

        relationalExpression();

        t = peekNextToken();
    }

    return dtype;
};


void returnStatement(char *retType){
    t = getNextToken();
    if (!strcmp(t.lexeme, "return")) {
        // is return keyword
    } else {
        char *errMsg = "Expected return keyword";
        error(t, errMsg);
    }

    t = peekNextToken();
    if(strcmp(t.lexeme, ";")) {
        // Check whether return type matches
        unk_type *ut_id = expression();
        if(strcmp(ut_id->type, "unknown") == 0) {
            strcpy(ut_id->type, retType);
            addUnknown(ut_id, unknown_ids, TYPE);
        } else {
            if(strcmp(retType, ut_id->type) != 0) {
                char *errMsg = "Incorrect return type";
                sem_error(t, errMsg);
            }
        } 
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, ";")) {
        // is ; symbol
    } else {
        char *errMsg = "Expected ';' symbol";
        error(t, errMsg);
    }


};

void doStatement() {
    t = getNextToken();
    if (!strcmp(t.lexeme, "do")) {
        // is do keyword
    } else {
        char *errMsg = "Expected 'do' keyword";
        error(t, errMsg);
    }

    subroutineCall();

    t = getNextToken();
    if (!strcmp(t.lexeme, ";")) {
        // is ; symbol
    } else {
        char *errMsg = "Expected ';' symbol";
        error(t, errMsg);
    }

};

void whileStatement() {
    t = getNextToken();
    if (!strcmp(t.lexeme, "while")) {
        // is while keyword
    } else {
        char *errMsg = "Expected 'while' keyword";
        error(t, errMsg);
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "(")) {
        // is ( symbol
    } else {
        char *errMsg = "Expected '(' symbol";
        error(t, errMsg);
    }

    expression();

    t = getNextToken();
    if (!strcmp(t.lexeme, ")")) {
        // is ) symbol
    } else {
        char *errMsg = "Expected ')' symbol";
        error(t, errMsg);
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "{")) {
        // is { symbol
    } else {
        char *errMsg = "Expected '{' symbol";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(cmp(t.lexeme, stmt_array, 6)) {
        statement();
        t = peekNextToken();
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "}")) {
        // is } symbol
    } else {
        char *errMsg = "Expected '}' symbol";
        error(t, errMsg);
    }

};


void ifStatement() {
    t = getNextToken();
    if (!strcmp(t.lexeme, "if")) {
        // is keyword if
    } else {
        char *errMsg = "Expected if keyword";
        error(t, errMsg);
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "(")) {
        // is ( symbol
    } else {
        char *errMsg = "Expected '(' symbol";
        error(t, errMsg);
    }

    expression();

    t = getNextToken();
    if (!strcmp(t.lexeme, ")")) {
        // is ) symbol
    } else {
        char *errMsg = "Expected ')' symbol";
        error(t, errMsg);
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "{")) {
        // is { symbol
    } else {
        char *errMsg = "Expected '{' symbol";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(cmp(t.lexeme, stmt_array, 6)) {
        statement();
        t = peekNextToken();
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "}")) {
        // is } symbol
    } else {
        char *errMsg = "Expected '}' symbol";
        error(t, errMsg);
    }

    t = peekNextToken();
    if(!strcmp(t.lexeme, "else")) {
        getNextToken();

        t = getNextToken();
        if (!strcmp(t.lexeme, "{")) {
            // is { symbol
        } else {
            char *errMsg = "Expected '{' symbol";
            error(t, errMsg);
        }

        t = peekNextToken();
        while(cmp(t.lexeme, stmt_array, 6)) {
            statement();
            t = peekNextToken();
        }

        t = getNextToken();
        if (!strcmp(t.lexeme, "}")) {
            // is } symbol
        } else {
            char *errMsg = "Expected '}' symbol";
            error(t, errMsg);
        }
    }
};

void letStatement() {
    t = getNextToken();
    unk_type *dtype;
    char *lhs_type;

    if (!strcmp(t.lexeme, "let")) {
        // is let keyword
    } else {
        char *errMsg = "Expected let keyword";
        error(t, errMsg);
    }

    t = getNextToken();
    sym *currSym = lookUp(t.lexeme, currSymTb);
    if (t.type == identifier) {
        // check if identifier is declared 
        if(currSym == NULL) {
            char *errMsg = "Error: variable has not been previously declared";
            error(t, errMsg);
        }
        // Set initialized boolean of symbol
        currSym->init = 1;
        lhs_type = currSym->dtype;
    } else {
        char *errMsg = "Expected an identifier";
        error(t, errMsg);
    }

    t = peekNextToken();
    if(!strcmp(t.lexeme, "[")) {
        getNextToken();

        // Check that identifier is of type array
        if (strcmp(currSym->dtype, "Array") == 0) {
            dtype = expression();
            // Check that the expression is a number for array
            if (strcmp(dtype->type, "int") != 0 && strcmp(dtype->type, "char") != 0) {
                char *errMsg = "Expression for array indices does not evalute to an integer";
                sem_error(t, errMsg);
            }
        } else {
            char *errMsg = "Identifier is not of type Array";
            error(t, errMsg);
        }

        t = getNextToken();

        if(!strcmp(t.lexeme, "]")) {
            // Symbol is ]
        } else {
            char *errMsg = "Expected ']' symbol";
            error(t, errMsg);
        }
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, "=")) {
        // is = symbol
    } else {
        char *errMsg = "Expected '=' symbol";
        error(t, errMsg);
    }

    dtype = expression();
    // Check if LHS data type matches RHS data type
    // Exceptions for array item or char and int
    // If type unknown check later
    if (strcmp(lhs_type, "Array") != 0 && strcmp(dtype->type, "Array")!= 0) {
        if (strcmp(dtype->type, "unknown") == 0) {
            strcpy(dtype->type, lhs_type);
            addUnknown(dtype, unknown_ids, TYPE);
        } else if (strcmp(lhs_type, "char") == 0 || strcmp(lhs_type, "int") == 0) {
            if (strcmp(dtype->type, "char") != 0 && strcmp(dtype->type, "int") != 0) {
                char *errMsg = "Assignment of uncompatible types";
                sem_error(t, errMsg);
            }
        } else {
            if(strcmp(lhs_type, dtype->type) != 0) {
                char *errMsg = "Assignment of uncompatible type";
                error(t, errMsg);
            }
        }
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, ";")) {
        // is ; symbol
    } else {
        char *errMsg = "Expected ';' symbol";
        error(t, errMsg);
    }

};

void varDeclarStatement() {
    t = getNextToken();
    sym *newSym;
    char *dtype;

    if (!strcmp(t.lexeme, "var")) {
        // is var keyword
    } else {
        char *errMsg = "Expected var keyword";
        error(t, errMsg);
    }

    dtype = type();

    t = getNextToken();
    if (t.type == identifier) {
        // Check if symbol is already declared prev
        if(lookUp(t.lexeme, currSymTb) != NULL) {
            char *errMsg = "Variable already previously declared";
            error(t, errMsg);
        }

        // Init new sym
        newSym = init_sym();
        newSym->name = strdup(t.lexeme);
        newSym->kind = VAR;
        newSym->offset = currSymTb->count[VAR];
        newSym->dtype = strdup(dtype);
        append(newSym, currSymTb);

    } else {
        char *errMsg = "Expected an identifier";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(!strcmp(t.lexeme, ",")) {
        getNextToken();
        t = getNextToken();

        if (t.type == identifier) {
            // Check if symbol is already declared prev
            if(lookUp(t.lexeme, currSymTb) != NULL) {
                char *errMsg = "Variable already previously declared";
                error(t, errMsg);
            }

            // Init new sym
            newSym = init_sym();
            newSym->name = strdup(t.lexeme);
            newSym->kind = VAR;
            newSym->offset = currSymTb->count[VAR];
            newSym->dtype = strdup(dtype);
            append(newSym, currSymTb);

        } else {
            char *errMsg = "Expected an identifier";
            error(t, errMsg);
        }

        t = peekNextToken();
    }

    t = getNextToken();
    if (!strcmp(t.lexeme, ";")) {
        // is ; symbol
    } else {
        char *errMsg = "Expected ';' symbol";
        error(t, errMsg);
    }


};

int statement() {
    t = peekNextToken();
    int returned = 0;

    if (!strcmp(t.lexeme, "var")) {
        varDeclarStatement();
    } else if (!strcmp(t.lexeme, "let")) {
        letStatement();
    } else if (!strcmp(t.lexeme, "if")) {
        ifStatement();
    } else if (!strcmp(t.lexeme, "while")) {
        whileStatement();
    } else if (!strcmp(t.lexeme, "do")) {
        doStatement();
    } else if (!strcmp(t.lexeme, "return")) {
        sym *s = lookUp(currSymTb->name, currSymTb);
        returnStatement(s->retType);
        returned = 1;
    } else {
        char *errMsg = "Expected statement keyword";
        error(t, errMsg);
    }
    return returned;
}


int subroutineBody() {
    t = getNextToken();
    int returned = 0;
    if(!strcmp(t.lexeme, "{")){
        // symbol: {
    } else {
        char *errMsg = "Expected '{' symbol";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(cmp(t.lexeme, stmt_array, 6)) {
        returned = statement();
        // Check for unreachable code after return
        t = peekNextToken();
        if (returned && strcmp(t.lexeme, "}")) {
            char *errMsg = "Unreachable code after return";
            sem_error(t, errMsg);
        }
    }

    t = getNextToken();
    if(!strcmp(t.lexeme, "}")){
        // symbol: }
    } else {
        char *errMsg = "Expected '}' symbol";
        error(t, errMsg);
    }
    return returned;
};

/* Function: paramList()
 * return: the numbers of parameters for function */
int paramList() {
    t = peekNextToken();
    int count = 0;
    sym *newSym = init_sym();

    if (!cmp(t.lexeme, type_array, 4) && !cmp(t.lexeme, newType, type_counter)
            && !cmp(t.lexeme, api_array, 8)) return 0;

    char *dtype = type();

    t = getNextToken();
    if (t.type == identifier) {
        count += 1;

        // Init new sym
        newSym->name = strdup(t.lexeme);
        newSym->kind = ARG;
        newSym->offset = currSymTb->count[ARG];
        newSym->dtype = strdup(dtype);
        append(newSym, currSymTb);

    } else {
        char *errMsg = "Expected to find an identifier";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(!strcmp(t.lexeme, ",")) {
        getNextToken();
        dtype = type();

        t = getNextToken();
        if (t.type == identifier) {
            count += 1;

            // Check if symbol is already declared prev
            if(lookUp(t.lexeme, currSymTb) != NULL) {
                char *errMsg = "Parameter already previously declared";
                error(t, errMsg);
            }

            // Init new sym
            newSym = init_sym();
            newSym->name = strdup(t.lexeme);
            newSym->kind = ARG;
            newSym->offset = currSymTb->count[ARG];
            newSym->dtype = strdup(dtype);
            append(newSym, currSymTb);

        } else {
            char *errMsg = "Expected to find an identifier";
            error(t, errMsg);
        }

        t = peekNextToken();
    }

    return count;
};


void subroutineDeclar() {
    t = getNextToken();
    sym *newSym = init_sym();
    char * retType =  "";
    enum kind sym_kind;

    if (!strcmp(t.lexeme, "constructor")) {
        sym_kind = CONSTRUCTOR;
    } else if(!strcmp(t.lexeme, "function")) {
        sym_kind = FUNCTION;
    } else if(!strcmp(t.lexeme, "method")) {
        sym_kind = METHOD;
    } else {
        char *errMsg = "Expected keywords: constructor, function, or method";
        error(t, errMsg);
    }

    t = peekNextToken();
    if (!strcmp(t.lexeme, "void")) {
        getNextToken();
        // void keyword	
    } else {
        retType = type();
    }

    t = getNextToken();
    if (t.type == identifier) {
        // Check if identifier has already been declared
        if(lookUp(t.lexeme, currSymTb) != NULL) {
            char *errMsg = "Variable already declared in class";
            error(t, errMsg);
        }


        // Init new sym
        newSym->name = strdup(t.lexeme);
        newSym->kind = sym_kind;
        newSym->offset = currSymTb->count[sym_kind];
        newSym->retType = retType;
        newSym->scope = init_symTb();
        append(newSym, currSymTb);

    } else {
        char *errMsg = "Expected an identifier";
        error(t, errMsg);
    }

    // Change currScope to subroutine, init symTb
    newSym->scope->prev = currSymTb;
    currSymTb = newSym->scope;
    currSymTb->name = strdup(t.lexeme);

    t = getNextToken();
    if(!strcmp(t.lexeme, "(")) {
        // '('
        newSym->args = paramList();
    } else {
        char *errMsg = "Expected symbol '('";
        error(t, errMsg);
    }

    t = getNextToken();
    if(!strcmp(t.lexeme, ")")) {
        // ')'
        // Check for return statement in function
        if( subroutineBody() == 0 && sym_kind == FUNCTION) {
            char *errMsg = "No return statement in function";
            sem_error(t, errMsg);
        }
    } else {
        char *errMsg = "Expected symbol ')'";
        error(t, errMsg);
    }

    // Return to prev scope
    currSymTb = currSymTb->prev;
}; 

void classVarDeclar() {
    t = getNextToken();
    sym *newSym = init_sym();
    enum kind stype;

    if (!strcmp(t.lexeme, "static")) {
        // static
        stype = STATIC;
    } else if (!strcmp(t.lexeme, "field")) {
        // field
        stype = FIELD;
    } else {
        char *errMsg = "Expected 'static' or 'field' key word";
        error(t, errMsg);
    }

    char *dtype = type();

    t = getNextToken();
    if (t.type == identifier) {
        if(lookUp(t.lexeme, currSymTb) != NULL) {
            char *errMsg = "Variable already declared in class";
            error(t, errMsg);
        }

        // Init values for new sym
        newSym->name = strdup(t.lexeme);
        newSym->kind = stype;
        newSym->offset = currSymTb->count[stype];
        newSym->dtype = strdup(dtype);
        append(newSym, currSymTb);

    } else {
        free(newSym);
        char *errMsg = "Expected identifier";
        error(t, errMsg);
    }

    t = peekNextToken();
    while(!strncmp(t.lexeme, ",", 1)) {
        getNextToken();
        t = peekNextToken();
        if (t.type == identifier) {
            if(lookUp(t.lexeme, currSymTb) != NULL) {
                char *errMsg = "Variable already declared in class";
                error(t, errMsg);
            }
            // Init new sym
            newSym = init_sym();
            newSym->name = strdup(t.lexeme);
            newSym->kind = stype;
            newSym->offset = currSymTb->count[stype];
            newSym->dtype = strdup(dtype);
            append(newSym, currSymTb);

            // Consume token
            getNextToken();
            t = peekNextToken();
        } else {
            free(newSym);
            char *errMsg = "Expected an identifier";
            error(t, errMsg);
        }
    }

    t = getNextToken();
    if(!strncmp(t.lexeme, ";", 1)) {
        // semi-colon
    } else {
        char *errMsg = "Expected ';' to end declaration";
        error(t, errMsg);
    }
};

void memberDeclar() {
    t = peekNextToken();
    if (cmp(t.lexeme, class_declar, 2)) {
        classVarDeclar();
    } else if (cmp(t.lexeme, sub_declar, 3)) {
        subroutineDeclar();
    } else {
        char *errMsg = "Expected class variable declaration or subroutine declaration";
        error(t, errMsg);
    }
    return;
}

void classDeclar() {
    t = getNextToken();

    if (!strncmp(t.lexeme, "class", 4)) {
        // Add later
    } else {
        char * errMsg = "class keyword is exepected";
        error(t, errMsg);
    }

    t = getNextToken();
    if (t.type == identifier) {
        //TODO: Get rid of later
        newType[type_counter] = t.lexeme;
        type_counter += 1;

        // Create new sym and add to table
        sym *newSym = init_sym();
        newSym->name = strdup(t.lexeme);
        newSym->kind = CLASS;
        newSym->scope = init_symTb();
        append(newSym, currSymTb);

        // Change to class scope
        newSym->scope->prev = currSymTb;
        currSymTb = newSym->scope;
        currSymTb->name = strdup(t.lexeme);


    } else {
        char *errMsg = "identifier is expected";
        error(t, errMsg);
    }

    t = getNextToken();
    if (!strncmp(t.lexeme, "{", 1)) {
        // Add later
    } else {
        char *errMsg = "expected {";
        error(t, errMsg);
    }

    t = peekNextToken();
    while (cmp(t.lexeme, class_declar, 2) || cmp(t.lexeme, sub_declar, 3)) {
        memberDeclar();
        t = peekNextToken();
    }

    t = getNextToken();
    if (!strncmp(t.lexeme, "}", 1)) {
        // Add later
    } else {
        char *errMsg = "Expected '}' or declaration keyword";
        error(t, errMsg);
    }

    // Exit out of scope
    currSymTb = currSymTb->prev;
    return;
}

/* Initialize the parser */
int init_parser(char *dirName, DIR **dr){
    int fileCount = 0;
    int x = 0;

    if(init_lexer(dirName, &fileCount, dr) != 0) return -1;
    printf("Parser Initialized\n");

    /* Initialize global symTb and unknown symbols arr*/
    glbSymTb = init_symTb();
    currSymTb = glbSymTb;
    unknown_ids = calloc(1, sizeof(unknown_arr));

    while(x < fileCount) {
        classDeclar();

        token tk = peekNextToken();
        if (tk.type == eof) {
            getNextToken(); // Consume EOF
        }

        x += 1;
    }

    resolve(unknown_ids, glbSymTb);
    resolveType(unknown_ids);
    resolveArgs(unknown_ids);
   // print(glbSymTb);
}

/* Error Message generating function */
void sem_error(token t, char *errMsg) {
    fprintf(stderr, "Semantic Error in %s:line %d, at or near '%s'\nType: %s\n\n", t.file, t.lineNum, t.lexeme, errMsg);
    return;
}
