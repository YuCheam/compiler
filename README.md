First run the executable file make to create the compiler executable by typing:
./make
This compiles all the code into the executable file main

To run the compiler on a Jack directory, type
./main JackDirectoryName

**NOTE: all classes, including JACK API files, that are used in the program must be in the directory or it will not compile correctly. 
For example, if Fraction.jack file uses Output and Array in its code the the Output.jack file and Array.jack file also need to be in the directory.
