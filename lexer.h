#ifndef LEXER_H__
#define LEXER_H__

# include <stdio.h>
# include <stdlib.h>
# include <assert.h>
# include <ctype.h>
# include <string.h>
# include <dirent.h>

enum TokenTypes {
	symbol = 0, 
	keyword = 1, 
	number = 2, 
	identifier = 3,
	stringLiteral = 4,
	eof = 5
};

typedef struct Token {
	char* lexeme;
	enum TokenTypes type;
	char* file;
	int lineNum;
} token;

int init_lexer(char *dirName, int *fileCount, DIR **dr);
void close_file(FILE **fp);
token getNextToken();
token peekNextToken();
void error(token t, char *errMsg);

#endif
