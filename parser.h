#ifndef PARSER_H__
#define PARSER_H__

# include "symbolResolution.h"

int init_parser(char *dirName, DIR **dr);
int statement();
unk_type *expression();
int expressionList(void *p, int isKnown);
void sem_error(token t, char *errMsg);

#endif
