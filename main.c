# include "parser.h"
# include <dirent.h>

int main(int argc, char *argv[]) {
	char *line = NULL;
	assert(argc == 2);

	// open directory
	DIR *dr = opendir(argv[1]);
	if (dr == NULL) {
		printf("Could not open directory\n");
		exit(-1);
	}


	/* Initialize Parser */
	if(init_parser(argv[1], &dr) == -1) exit(-1);


	/* Exit */
	exit(0);
}
