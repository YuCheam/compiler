# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include "symbolTable.h"

/* init_symTb()
 * Function: create a new symbol table and initialize all values to zero
 * Return: pointer to the symbol table */
symTb *init_symTb() {
    symTb *newSymTb = malloc(sizeof(symTb));

    newSymTb->count = (int *)calloc(4, sizeof(int));
    newSymTb->head = NULL;
    newSymTb->prev = NULL;
    newSymTb->name = "";

    return newSymTb;
};

/* init_sym()
 * Function: Create an new symbol and initialize all values to zero
 * Return: pointer to new sym */
sym *init_sym() {
    sym *newSym = malloc(sizeof(sym));

    newSym->name = "";
    newSym->dtype = "";
    newSym->retType = "";
    newSym->scope = NULL;
    newSym->next = NULL;

    newSym->offset = 0;
    newSym->kind = -1;
    newSym->args = 0;
    newSym->init = 0;

    return newSym;

};

/* Function: append() -- adds symbol to table and 
 *    updates offset of symbol and count of table
 * Parameters: sym *s, symTb *t
 * Return: void */
void append(sym *s, symTb *t) {

    if (t->head == NULL) {
        t->head = s;
    } else {
        sym *curr = t->head;

        while (curr->next != NULL) {
            curr = curr->next;
        }

        curr->next = s;
    }

    if (s->kind == STATIC || s->kind == FIELD || s->kind == VAR || s->kind == ARG) {
        t->count[ s->kind] += 1;
    }
}


/* Function: lookUp() -- Look a symbol in symbol table
 * Parameter: char *name -- name of symbol
 *            symTb *t -- symbol table to lookup symbol
 * Return: NULL if no symbol is found, pointer to symbol if found */
sym *lookUp(char *name, symTb *t) {
    sym *curr;
    symTb *currTb = t;

    if (t == NULL) {
        return NULL;
    }

    while(currTb != NULL) {
        curr = currTb->head;

        while (curr != NULL && strcmp(curr->name, name)) {
            curr = curr->next;
        }

        if (curr != NULL) {
            return curr;
        }

        currTb = currTb->prev;
    }

    return NULL;
};


/* Function: findClass() -- Returns the class name of scope
 * Parameters: symTb *tb -- currSymTb
 * Return: char * 
 */
char *findClass(symTb *tb) {
    symTb *prevTb = tb;
    symTb *currTb = tb->prev;
    sym *s = currTb->head;

    while(s->kind != CLASS) {
        prevTb = tb->prev;
        currTb = prevTb->prev;
        s = currTb->head;
    } 

    while(s->scope != prevTb) {
        s = s->next;
    }

    return s->name;
}

/* Function: findTopScope() -- Finds the symbol table of class scope
 * Parameters: symTb *tb -- current scope
 * Return: symTb *tb -- Top level scope */
symTb *findTopScope(symTb *tb) {
    symTb *prevTb = tb;
    symTb *currTb = tb->prev;
    sym *s = currTb->head;

    while(s->kind != CLASS) {
        prevTb = tb->prev;
        currTb = prevTb->prev;
        s = currTb->head;
    } 

    return prevTb;

}

/* Function: print() -- prints all the symbol table */
void print(symTb *t) {
    sym *curr = t->head;

    while (curr != NULL) {
        if(curr->kind == VAR || curr->kind == FIELD || curr->kind == STATIC || curr->kind == ARG ) {
            printf("Name: %s, Offset: %d, Datatype: %s\n", curr->name, curr->offset, curr->dtype);
        } else if (curr->kind == CLASS) {
            printf("Name: %s, Offset: %d, is a class\n", curr->name, curr->offset);
        } else {
            printf("Name: %s, Offset: %d, ArgNum: %d, retType: %s\n", curr->name, curr->offset, curr->args, curr->retType);
        }

        if (curr->scope != NULL) {
            printf("Entering subroutine: %s\n", curr->name);
            print(curr->scope);
            printf("Leaving subroutine\n \n");
        }

        curr = curr->next;
    }
};


