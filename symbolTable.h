#ifndef SYMBOLTABLE_H__
#define SYMBOLTABLE_H__

enum kind {
    STATIC = 0,
    FIELD = 1,
    ARG = 2,
    VAR = 3,
    CLASS = 4,
    METHOD = 5,
    FUNCTION = 6,
    CONSTRUCTOR = 7
};

typedef struct Symbol {
    char* name;
    int offset; // The relative address (offset) of the variable
    enum kind kind;

    // data type if variable
    char *dtype;
    int init; // If initialized to a value 1, uninitialized 0

    // Function info
    int args;
    char *retType;

    struct symbolTable *scope;
    struct Symbol *next;
} sym;

typedef struct symbolTable {
    sym *head;
    struct symbolTable *prev;
    char *name;
    int *count;
} symTb;

// Symbol Table Functions
void append(sym *s, symTb *t);
sym *lookUp(char *name, symTb *t);
void print(symTb *t);
char *findClass(symTb *tb);
symTb *findTopScope(symTb *tb);

symTb *init_symTb();
sym *init_sym();

#endif
