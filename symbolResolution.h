#ifndef SYMBOLRESOLUTION_H__
#define SYMBOLRESOLUTION_H__
#include "lexer.h"
#include "symbolTable.h"

enum unk_case {
	ID = 0,
	TYPE = 1,
	ARGNUM = 2
};

typedef struct {
  token t;
  char *type;
  sym **s;
} unknown_id;

typedef struct {
  unknown_id *id;
  char *type;
} unk_type;

typedef struct{
  unknown_id *id;
  int args;
} unk_nargs;

typedef struct {
    unknown_id *unknown[100];
	unk_type *unkType[100];
    unk_nargs *unkArgs[100];
    int total[3];
} unknown_arr;

void addUnknown(void *id, unknown_arr *arr, enum unk_case u_case);
void resolve(unknown_arr *arr, symTb *tb);
void resolveType(unknown_arr *arr);
void resolveArgs(unknown_arr *arr);

unknown_id *init_unknownId(token t, char* type);

#endif
