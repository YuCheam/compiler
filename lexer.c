# include "lexer.h"

/* Global Structs and Definitions */
#define CR 13
#define NL 10


static const char *tokenString[] = {"symbol", "keyword", "number", 
	"identifier", "stringLiteral", "eof"};


token tokenBuf[1000000];
int currToken = 0;

int line = 1;
char *currFile;
int symbols[] = {38, 40, 41, 42, 43, 44, 45, 46, 47, 59, 60, 61, 62, 91, 93, 123, 124, 125, 126};

int startKeywords[] = {98, 99, 100, 101, 102, 105, 108, 109, 110, 114, 115, 116, 118, 119}; // length = 14

const char *bdKW[] = {"boolean", "class", "constructor", "char", "do"};
const char *efKW[] = {"else", "false", "field", "function"};
const char *inKW[] = {"if", "int", "let", "method", "null"};
const char *rtKW[] = {"return", "static", "this", "true"};
const char *wvKW[] = {"while", "void", "var"};


/* Implement peek function */
char peek(FILE *fp) {
	char c;
	c = fgetc(fp);
	ungetc(c, fp);
	
	return c;
}

/* Closes file pointer */
void close_file(FILE **fp) {
	fclose(*fp);
	return;
}

/* Error Message generating function */
void error(token t, char *errMsg) {
	fprintf(stderr, "Error in %s:line %d, at or near '%s'\nType: %s\n", t.file, t.lineNum, t.lexeme, errMsg);
	printf("I am crossed :( bye bye\n");
	exit (-1);
}

/* findKeyword
 * param: *char find, int len
 * return: 0 not found, 1 found
 * finds keyword in list */
int findKeyword(char *find) {
	int firstChar = (int) find[0];
	int i = 0;
	int found = 1;

	if (firstChar >= 98 && firstChar <= 100) {
		while(i < 5 && (found = strcmp(find, bdKW[i])) ) {
			i++;
		}
	} else if (firstChar == 101 || firstChar == 102) {
		while( i < 4 && (found = strncmp(find, efKW[i], strlen(efKW[i]))) ) {
			i++;
		}
	} else if (firstChar >= 105 && firstChar <= 110) {
		while(i < 5 && (found = strcmp(find, inKW[i])) ) {
			i++;
		}
	} else if (firstChar >= 114 && firstChar <= 116) {
		while(i < 4 && (found = strncmp(find, rtKW[i], strlen(rtKW[i]))) ) {
			i++;
		 }
	} else {
		while(i < 3 && (found = strncmp(find, wvKW[i], strlen(wvKW[i]))) ) {
			i++;
		 }
	}

	return !found;
}

/* Search for num in list
 * Uses binary search algorithm 
 * Returns 1 if found 0 if not found */
int search(int find, int search[], int length) {
	int l = 0;
	int r = length - 1;
	int mid;

	while (l <= r) {
		mid = (r + l)/ 2;
		if(find == search[mid]) return 1;
		
		if (find < search[mid]) {
			r = mid - 1;
		} else {
			l = mid + 1;
		}
	}

	return 0;
}

/* Gets rid of whitespace */
int consumeWS(int* c, FILE *fp) {
	int temp = 0;
	/* Get rid of whitespace */
	while ( (*c != EOF) && isspace( (char) *c)) {
		if ( *c == CR || *c == NL) { // Carriage Return or NewLine
			line += 1;
			if (*c == CR) fgetc(fp); // consume NL after CR
		}
		fgetc(fp);
		*c = peek(fp);
		temp = 1;
	}

	return temp;
}

void consumeComments(int* c, FILE *fp) {
	int temp;
	int WS;

	while (*c == 0x2f || isspace((char) *c)) {
		WS = consumeWS(c, fp);

		if (WS) {
			temp = WS;
		} else {
			*c = fgetc(fp);
			temp = peek(fp);
		}
		
		switch (temp) {
			case 1:
				break;
			case 0x2f: // 0x2f forward slash
				//printf("Comment till end of line %d\n", line);
				while (*c != CR && *c != NL) {
					fgetc(fp);
					*c = peek(fp);
				}
				
				line += 1;
				if (*c == CR) fgetc(fp); // consume NL after CR
				fgetc(fp); //consume NewLine get next char
				*c = peek(fp);

				break;
		
			case 42: // 42 = '*'
				fgetc(fp);
				*c = peek(fp);

				//printf("Comment til closing, line %d\n", line);
				while (*c != EOF) {
					if (*c == '*') {
						fgetc(fp); 
						*c = peek(fp);
						if (*c == '/') break;
					}

					if ( *c == CR || *c == NL) { // Carriage Return or NewLine
						line += 1;
						if (*c == CR) fgetc(fp); // consume NL after CR
					}

					fgetc(fp);
					*c = peek(fp);
				}
				
				if (*c == EOF) {
					char *errMsg = "EOF in multi-line comment";
					char *lexeme = "EOF";
					token t;
					t.lexeme = (char *) malloc(3);
					strncpy(t.lexeme, lexeme, 3);
					t.lineNum = line;
					error(t, errMsg);
				}

				fgetc(fp);
				*c = peek(fp);

				break;
			default: //When it is just a division sign
				temp = -5;
		}

		/* In the case of division sign */
		if (temp == -5) break;
	}
	return;
}

/* getToken()
 * Param: FILE *fp
 * Return: token
 * Function: will consume and return the next token
 */
token getToken(FILE *fp) {
	token newToken;
	int c = peek(fp);
	
	/* Get rid of comments and whitespace */
	consumeComments(&c, fp);
	
	/* Find Numbers */
	if (isdigit(c)) {
		char num[50] = "";
		int len = 0;
		while (c != EOF && isdigit(c)) {
			num[len] = (char) c;
			len += 1;
			fgetc(fp);
			c = peek(fp);
		}
		
		// Initialize token
		newToken.lexeme = (char *) malloc(len);
		strncpy(newToken.lexeme, num, len);
		newToken.file = (char *) malloc(strlen(currFile));
		strcpy(newToken.file, currFile);
		newToken.type = number;
		newToken.lineNum = line;

		//printf("lexeme: %s, type: number, file: %s,line: %d\n", newToken.lexeme, newToken.file, newToken.lineNum);
		return newToken;
	}

	/* Find String Literal */
	if (c == 34) {
		fgetc(fp); // Consume
		c = peek(fp);
		char buf[1000]; // TODO: Figure out better way 
		int len = 0;
		
		// NOTE: This does not save the quotations
		while (c != 34 && c != EOF) {
			buf[len] = (char) c;
			len += 1;
			
			fgetc(fp);
			c = peek(fp);
		}
		
		if (c == EOF) {
			char *msg = "EOF before closure of string literal";
			char *lexeme = "EOF";
			newToken.lexeme = (char *)malloc(3);
			strncpy(newToken.lexeme, lexeme, 3);
			newToken.lineNum = line;
			error(newToken, msg);
		}

		fgetc(fp);
		c = peek(fp);

		// Init Token
		newToken.lexeme = (char *) malloc(len);
		strncpy(newToken.lexeme, buf, len);
		newToken.file = (char *) malloc(strlen(currFile));
		strcpy(newToken.file, currFile);
		newToken.type = stringLiteral;
		newToken.lineNum = line;
		
		//printf("lexeme: %s, type: stringLiteral, line: %d\n", newToken.lexeme, newToken.lineNum);
		return newToken;
	}

	/* Find Identifiers */
	if (isalpha(c) || c == 95 ) { // 95 = _
		int isID = 0;
		char buf[1000];
		int len = 0;

		while(c != EOF && (isalpha(c) || isdigit(c) || c == 95)) {
			if (isdigit(c) || c == 95 || isupper(c)) isID = 1;
			
			buf[len] = (char) c;
			len += 1;

			fgetc(fp);
			c = peek(fp);
		}

		if (isID || !search((int) buf[0], startKeywords, 14)) {
			newToken.lexeme = (char *) malloc(len);
			strncpy(newToken.lexeme, buf, len);
			newToken.file = (char *) malloc(strlen(currFile));
			strcpy(newToken.file, currFile);
			newToken.type = identifier;
			newToken.lineNum = line;
			//printf("lexeme: %s, type: id, line: %d\n", newToken.lexeme, newToken.lineNum);
			return newToken;

		}

		if (findKeyword(buf)) {
			newToken.lexeme = (char *) malloc(len);
			strncpy(newToken.lexeme, buf, len);
			newToken.file = (char *) malloc(strlen(currFile));
			strcpy(newToken.file, currFile);
			newToken.type = keyword;
			newToken.lineNum = line;
			//printf("lexeme: %s, type: keyword, line: %d\n", newToken.lexeme, newToken.lineNum);
			return newToken;
		}

		newToken.lexeme = (char *) malloc(len);
		strncpy(newToken.lexeme, buf, len);
		newToken.file = (char *) malloc(strlen(currFile));
		strcpy(newToken.file, currFile);
		newToken.type = identifier;
		newToken.lineNum = line;
		//printf("lexeme: %s, type: id, line: %d\n", newToken.lexeme, newToken.lineNum);
		return newToken;
	}

	/* Find Symbols */
	if (search(c, symbols, 19)) {
		if (c != 47) fgetc(fp);

		newToken.lexeme = (char *) malloc(sizeof(char));
		strncpy(newToken.lexeme, (char *) &c, sizeof(char));
		newToken.file = (char *) malloc(strlen(currFile));
		strcpy(newToken.file, currFile);
		newToken.type = symbol;
		newToken.lineNum = line;

		//printf("lexeme: %s, type: symbol, line: %d\n", newToken.lexeme, newToken.lineNum);
		return newToken;
	}

	/* Token for EOF */
	if (c == EOF) {
		char *lexeme = "EOF";
		
		newToken.lexeme = (char *) malloc (3);
		strncpy(newToken.lexeme, lexeme, 3);	
		newToken.file = (char *) malloc(strlen(currFile));
		strcpy(newToken.file, currFile);
		newToken.type = eof;
		newToken.lineNum = line;

		//printf("lexeme: %s, line: %d\n", newToken.lexeme, newToken.lineNum);
return newToken;
	}  else {
		char *errMsg = "Symbol not used in Jack";
		newToken.lexeme = (char *)malloc(sizeof(char));
		strncpy(newToken.lexeme, (char *) &c, sizeof(char));
		newToken.lineNum = line;
		error(newToken, errMsg);

	}

	return newToken;
}

/* Initializes lexer by iterating through files in directory */
int init_lexer(char *dirName, int *fileCount, DIR **dr) {
	struct dirent *de;
	FILE *fp;
	int i = 0;

	/* Creating the relative path */
	char path[200];
	int dirNameLen = strlen(dirName);
	
	memcpy(path, dirName, dirNameLen);

	// Check for ending /
	if (strncmp(&path[dirNameLen-1], "/", 1)) {
		memcpy(&path[dirNameLen], "/", 1);
		dirNameLen += 1;
	}

	
	/* Iterate through directory */
	while((de = readdir(*dr)) != NULL) {
		line = 1; // Reset line number for new file

		if(de->d_type == DT_REG) {
			char buf[200];
			memcpy(buf, path, dirNameLen+1);
			memcpy(&buf[dirNameLen], de->d_name, strlen(de->d_name)+1);

			/* Open file */
			fp = fopen(buf, "r");
			if (fp == NULL) {
				fprintf(stderr, "Cannot open file %s!\n", de->d_name);
				return -1;
			}

			*fileCount += 1;
			currFile = de->d_name;

			/* Reading char from file */
			token t = {.type = 0};
			while(t.type != eof) {
				t = getToken(fp);
				tokenBuf[i] = t;
				i += 1;
			}

			/* Close file */
			close_file(&fp);
		}
	}

	printf("Lexer is initialized, all files have been tokenized\n");

	return 0;
}


/* Function: getNextToken()
 * Param: void
 * Return: token
 * Purpose: Return next avaliable token but token is consumed */
token getNextToken() {
	token t = tokenBuf[currToken];
	currToken += 1;

	//printf("getNextToken(): token: %s, type: %s, line: %d\n", t.lexeme, tokenString[t.type], t.lineNum); 
	return t;
}


/* Function: peekNextToken()
 * Param: void
 * Return: token
 * Purpose: Return next avaliable token but token is not consumed */
token peekNextToken() {
	token t = tokenBuf[currToken];
	//printf("peekNextToken(): token: %s, type: %s, line: %d\n", t.lexeme, tokenString[t.type], t.lineNum); 
	return t;
}

